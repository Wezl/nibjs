// example state:
// source_string: ' `comment` "asdf" @: 0'
// types: [ "string", "name", ":", "number" ]
// values: [ "asdf", "@", ":", 0 ]
// positions: [ 11, 18, 19, 21 ]
// tn: the index of the current token e.g. the ":" is at index 2

export class Parser {
  source_string;
  types = [];
  values = [];
  positions = [];
  tn = 0;

  constructor(str) { this.source_string = str; }
  static from_string = str => {
    const parser = new Parser(str);
    parser.init_from_string();
    return parser;
  }

  init_from_string() {
    let str = this.source_string;
    const eat_whitespace = str => str.replace(/^(?:[ \t\n]|`[^`]+`|)+/,"");
    //str.replace(/^(?:[ \t\n]|(```+)(?: | .*? \1)|`[^`]+`|)+/,"");//considering
    const add_token = (type, value) => {
      this.types.push(type);
      this.values.push(value);
    }
    const total_length = str.length;
    str = eat_whitespace(str);
    let match;
    while (str) {
      if ( match = // https://stackoverflow.com/a/13340826/
            str.match(/^(-?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?)/) ) {
        add_token('number', parseFloat(match[1]));
      } else if (match = str.match(/^"((?:[^"]|"")+)"/)) {
        add_token('string', match[1].replace(/""/g, '"'));
      } else if (match = str.match(/^[.]"([^"]|"")"/)) {
        add_token('character', match[1][0]);
      } else if (match = str.match(/^([.:]:?|[()$])/)) {
        add_token(match[1], match[1]);
      } else if (match = str.match(/^([^.(){}:$ \n\t"`]+)/)) {
        add_token('name', match[1]);
      } else {
        this.error('Token expected.', total_length - str.length);
      }
      this.positions.push(total_length - str.length);
      str = eat_whitespace(str.slice(match[0].length));
    }
  }
  type(){ return this.types[this.tn]; }
  value(){ return this.values[this.tn]; }
  position(){ return this.positions[this.tn]; }

  advance() {
    this.tn++;
  }

  peek(...types) {
    return types.includes(this.type());
  }
  consume(type) {
    if (this.type() === type) {
      const val = this.value();
      this.advance();
      return val;
    } else {
      return null;
    }
  }
  consumed() { return this.values[this.tn-1]; }

  name_or_paren() {
    if (this.consume('name')) {
      let ret = ['name', this.consumed()];
      while (this.consume('::')) {
        if (this.consume('name')) {
          ret.push(this.consumed());
        } else {
          this.error('Field expected.');
        }
      }
      return ret;
    } else if (this.peek('(', '$')) {
      return this.parenthesis();
    } else {
      this.error('Expected name or open parenthesis or \'$\'.');
    }
  }

  static constant_tokens = ['number', 'string', 'character'];
  parse_value() {
    if (this.peek(...Parser.constant_tokens)) {
      const ret = [this.type(), this.value()]
      this.advance();
      return ret;
    } else if (this.consume('.')) {
      return ['delay', this.name_or_paren()];
    } else {
      this.error('Expected argument. SHOULD BE UNREACHABLE');
    }
  }

  argument() {
    if (this.consume(':')) {
      if (this.peek('name', '(', '$')) {
        return ['apply', this.name_or_paren(), []];
      }
    }
    return this.parse_value();
  }

  application() {
    let func = this.name_or_paren(), args = [];
    while (this.peek(':', '.', ...Parser.constant_tokens)) {
      args.push(this.argument());
    }
    return ['apply', func, args];
  }

  subexpression() {
    let constant = null, applications = [], app;
    if (this.peek('.', ...Parser.constant_tokens)) {
      constant = this.parse_value();
    }
    while (this.peek('name', '(', '$')) {
      applications.push(this.application());
    }
    return [constant, applications];
  }

  expression() {
    const vars = [], vals = [];
    let subexpr = this.subexpression();
    while (this.consume('.:')) {
      if (!this.consume('name')) {
        this.error('Target expected for assignment.');
      }
      vars.push(this.consumed());
      vals.push(['expression', ...subexpr]);
      subexpr = this.subexpression();
    }
    const ret = ['expression', ...subexpr];
    return vars.length !== 0 ? ['binding', vars, vals, ret] : ret;
  }

  parenthesis() {
    if (this.consume('(')) {
      const ret = this.expression();
      if (!this.consume(')')) {
        this.error('Close parenthesis expected.');
      }
      return ret;
    } else if (this.consume('$')) {
      return this.expression();
    } else {
      this.error('Parenthesis expected. SHOULD BE UNREACHABLE');
    }
  }

  static ParseError = class ParseError {
    message;
    parser;
    position;
    constructor(m,p,position) {
      this.message = m;
      this.parser = p;
      this.position = position;
    }

    static CONTEXTLINES = 2;
    // how many lines to show before the offending line
    
    pretty() {
      const p = this.parser;
      let str = this.message + '\n';
      const pos = this.position;
      const before = p.source_string.substring(0, pos).split('\n');
      const line = before.length;

      const digits = (line+'').length;
      const addline = (n, s) => str+=(n+'').padStart(digits, ' ')+'| '+s+'\n';

      for (const i = Math.max(line-Parser.CONTEXTLINES, 1); i < line; i++) {
        addline(i, before[i]);
      }

      const line_start = before[line-1],
        line_end = p.source_string.substring(pos).match(/([^\n]*?)(\n|$)/)[1];
      addline(line, line_start+line_end);
      str += ' '.repeat(digits+2+line_start.length)+'^';
      return str;
    }
  };
  error(message, position=this.position() ?? this.source_string.length-1) {
    throw new Parser.ParseError(message, this, position);
  }
  
  parse() {
    const expression = this.expression();
    if (this.tn !== this.values.length) {
      this.error('Unexpected token.');
    }
    return expression;
  }
}

export const parse = str => Parser.from_string(str).parse()
